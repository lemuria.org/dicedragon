import asyncio
import random
import discord
from discord import Member
from dotenv import load_dotenv
import os

client = discord.Client()
load_dotenv()
DISCORD_TOKEN = os.getenv("DISCORD_TOKEN")

@client.event
async def on_ready():
    print('logged in as user {}'.format(client.user.name))
    client.loop.create_task(status_task())


async def status_task():
    while True:
        await client.change_presence(activity=discord.Game('Dragon Eye'), status=discord.Status.online)
        await asyncio.sleep(3)
        await client.change_presence(activity=discord.Game('with dice...'), status=discord.Status.online)
        await asyncio.sleep(3)


@client.event
async def on_message(message):
    if message.author.bot:
        return

    print("received: {} from {}".format(message.content, message.author))


    if '!help' in message.content:
        await message.channel.send('**Dragon Eye dice roller help:**\r\n'
                                   '!help - show this help')

    if message.content.startswith('!test') or message.content.startswith('!t'):
        args = message.content.split(' ')
        await do_roll(message, args)


async def do_roll(message, args):
    if args[1] == '-5':
        dice = 7
        keep = 'low'
    elif args[1] == '-4':
        dice = 6
        keep = 'low'
    elif args[1] == '-3':
        dice = 5
        keep = 'low'
    elif args[1] == '-2':
        dice = 4
        keep = 'low'
    elif args[1] == '-1':
        dice = 3
        keep = 'low'
    elif args[1] == '0':
        dice = 2
        keep = 'high'
    elif args[1] == '1' or args[1] == '+1':
        dice = 3
        keep = 'high'
    elif args[1] == '2' or args[1] == '+2':
        dice = 4
        keep = 'high'
    elif args[1] == '3' or args[1] == '+3':
        dice = 5
        keep = 'high'
    elif args[1] == '4' or args[1] == '+4':
        dice = 6
        keep = 'high'
    elif args[1] == '5' or args[1] == '+5':
        dice = 7
        keep = 'high'
    else:
        # invalid ability value
        await message.channel.send('**Invalid !test String**\r\n'
                                   '!test (or !t) - (ability score) (optional: heroic dice) - e.g. !t +3 1 or !t -2')
        return

    keep_dice = 2

    if len(args) > 2:
        dice += int(args[2])
        keep_dice += int(args[2])

    roll = []
    for x in range(0, dice):
        roll.append(roll_die())

    result = sorted(roll)
    if keep == 'high':
        result.reverse()
    outcome = result[:keep_dice]

    last = -1
    critical = False
    for d in outcome:
        if d == last:
            critical = True
        last = d

    if critical:
        if last == 1:
            outcome = []
            for x in range(0, keep_dice):
                outcome.append(0)
        else:
            if keep_dice == dice:
                # roll an additional die
                outcome.append(roll_die())
                outcome.sort()
            else:
                outcome = result[:keep_dice+1]

    sum = 0
    for d in outcome:
        sum += d


    embed = discord.Embed(title='**Dragon Eye** Ability Test for {}'.format(message.author.name),
                          description='Rolling {}d10, keep {} {}'.format(dice, keep, keep_dice),
                          color=0x22a7f0)
    embed.add_field(name='Roll', value=format(roll), inline=False)
    embed.add_field(name='Outcome', value=format(outcome), inline=True)
    embed.add_field(name='Total', value=format(sum), inline=True)
    if critical:
        if (last == 1):
            embed.add_field(name='Additional Info', value='**Critical Fail**', inline=True)
        else:
            embed.add_field(name='Additional Info', value='**Critical Success**', inline=True)
    await message.channel.send(embed=embed)


def roll_die(sides=10):
    die = random.randint(1, sides)
    return die


client.run(DISCORD_TOKEN)

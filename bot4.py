import os
import asyncio
from dotenv import load_dotenv
import discord
from discord.ext import commands

async def run():
    print('Dice Dragon starting up...')
    load_dotenv()
    bot = Bot()

    try:
        await bot.start(os.getenv('DISCORD_TOKEN'))
    except KeyboardInterrupt:
        await db.close()
        await bot.logout()


class Bot(commands.Bot):
    def __init__(self):
        super().__init__(
            command_prefix=commands.when_mentioned_or('/', '!'),
            description='Dragon Eye discord bot'
        )

        # Load cogs
        for filename in os.listdir("./cogs"):
            if filename.endswith(".py"):
                name = filename[:-3]
                # Comment to disable errorhandler module
                if name == 'errorhandler':
                    continue
                self.load_extension(f"cogs.{name}")

    async def on_ready(self):
        print('logged in as user {}'.format(self.user.name))

#    async def on_command_error(self, ctx, error):
#        if isinstance(error, commands.CommandNotFound):
#            pass
#        if isinstance(error, commands.MissingRequiredArgument):
#            pass
#        else:
#            print(error)


# run the bot
loop = asyncio.get_event_loop()
loop.run_until_complete(run())

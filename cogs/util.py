import discord
from discord.ext import commands


class Util(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name='myid', aliases=['me'], help='Show your unique Discord-ID (used for manual character data).')
    async def myid(self, ctx):
        await ctx.send(f'Your unique Discord-ID is {ctx.author.id}, {ctx.author.name}.')


def setup(bot):
    bot.add_cog(Util(bot))

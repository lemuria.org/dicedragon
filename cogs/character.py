import discord
from discord.ext import commands
import asyncio
import aiofiles
import glob
from pathlib import Path
import re
import json
import sys
import os
import urllib.parse
import random # for the duplicate code in FIXME below
from difflib import get_close_matches


class Character(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.characters = {}


    async def fill(self, contents, var, value):
        return contents.replace(f'<!-- {var} -->', str(value))

    # async def fill_stats(self, contents, key, data):
    #     contents = await self.fill(contents, f'{key} Base', data["base"])
    #     if "current" in data:
    #         contents = await self.fill(contents, f'{key} Current', data["current"])
    #     else:
    #         contents = await self.fill(contents, f'{key} Current', data["base"])
    #
    #     if "regeneration" in data:
    #         contents = await self.fill(contents, f'{key} Reg', data["regeneration"])
    #     else:
    #         contents = await self.fill(contents, f'{key} Reg', '1')
    #
    #
    #     return contents


    async def write_char_sheet(self, character):
        # write out the charsheet

        try:
            async with aiofiles.open(f'./viewer/template.html', mode='r') as f:
                contents = await f.read()
                await f.close()
        except:
            print("Unexpected error: {}".format(sys.exc_info()))
            raise


        # Base Values
        contents = await self.fill(contents, 'Name', character["name"])
        contents = await self.fill(contents, 'Race', character["race"])

        # Abilities
        for name, value in character["abilities"].items():
            if value["rank"] > 0:
                rank = "+"+str(value["rank"])
            elif value["rank"] < 0:
                rank = "- "+str(abs(value["rank"]))
            else:
                rank = "± 0"
            contents = await self.fill(contents, f'{name} Rank', rank)
            if "heroic" in value:
                contents = await self.fill(contents, f'{name} Heroic', value["heroic"])
            else:
                contents = await self.fill(contents, f'{name} Heroic', '')
            if "specializations" in value:
                contents = await self.fill(contents, f'{name} Specialization', ', '.join(value["specializations"]))
            else:
                contents = await self.fill(contents, f'{name} Specialization', ' ')

        # Stats
        # contents = await self.fill_stats(contents, "Hitpoints", character["stats"]["Hitpoints"])
        # contents = await self.fill_stats(contents, "Focus", character["stats"]["Focus"])
        # contents = await self.fill_stats(contents, "Mana", character["stats"]["Mana"])
        # contents = await self.fill_stats(contents, "Reaction", character["stats"]["Reaction"])

        # Classes
        i = 1
        for c in character["classes"]:
            contents = await self.fill(contents, f'Class {i}', c["class"])
            contents = await self.fill(contents, f'Class {i} Level', c["level"])
            contents = await self.fill(contents, f'Class {i} Base', c["base"])
            i += 1

        i = 1
        for name, rank in character["skills"].items():
            contents = await self.fill(contents, f'Skill {i} Name', name)
            contents = await self.fill(contents, f'Skill {i} Rank', rank)
            i += 1


        try:
            async with aiofiles.open(f'./viewer/{character["shortname"]}_{character["owner"]}.html', mode='w') as f:
                await f.write(contents)
                await f.close()
        except:
            raise



    # TODO: some of these commands maybe limit to private chat ?

    @commands.command(name='list', aliases=['character-list', 'char-list', 'charlist', 'cl'], help='List all your characters')
    async def characters_list(self, ctx):
        print("loading character list...")
        dir = glob.glob(f"./data/{ctx.author.id}/*.json")
        if len(dir) > 0:
            await ctx.send(f'Characters stored for {ctx.author.name}:')
            for file in dir:
                name = Path(file).stem
                await ctx.send(f'  * {name}')
        else:
            await ctx.send(f'No characters are stored for you, {ctx.author.name}.')

    @commands.command(name='load', aliases=['character-load', 'char-load', 'charload', 'char'], help='Load character data. Parameter: Character shortname. Only one character at a time can be active')
    async def character_load(self, ctx, name):
        # sanitize name to prevent injection attacks
        name = "".join([c for c in name if re.match(r'[ \w]', c)])
        try:
            async with aiofiles.open(f'./data/{ctx.author.id}/{name}.json', mode='r') as f:
                contents = await f.read()
                await f.close()
            data = json.loads(contents)
            data["shortname"] = name
            await ctx.send(f'Character {name} loaded and activated.')
        except:
            await ctx.send(f'Error: Unable to load character {name}.')
            print("Unexpected error: {}".format(sys.exc_info()))
            raise
        self.characters[ctx.author.id] = data

    @commands.command(name='create', aliases=['character-create', 'char-create', 'charcreate', 'cc'], help='Create a new character. Parameters: shortname, race (human, dwarf, elf, gnome)')
    async def character_create(self, ctx, name, race):
        # sanitize name to prevent injection attacks
        name = "".join([c for c in name if re.match(r'[ \w]', c)])
        race = "".join([c for c in race if re.match(r'\w', c)])
        try:
            async with aiofiles.open(f'./data/base_{race}.json', mode='r') as f:
                contents = await f.read()
                await f.close()
        except:
            await ctx.send(f'Error: Invalid race {race}.')
            return

        character = json.loads(contents)
        character["name"] = name
        character["shortname"] = name
        character["race"] = race
        character["owner"] = ctx.author.id

        if glob.glob(f"./data/{ctx.author.id}/{name}.json"):
            await ctx.send(f'Error: You already have a character named {name}, {ctx.author.name}.')

        data = json.dumps(character, indent=3)
        try:
            datadir = f"./data/{ctx.author.id}/"
            if not glob.glob(datadir):
                os.makedirs(datadir, exist_ok=True)
            async with aiofiles.open(f'{datadir}{name}.json', mode='w') as f:
                await f.write(data)
                await f.close()
        except:
            await ctx.send(f'Error: Unable to create character.')

        self.characters[ctx.author.id] = character
        await ctx.send(f'New character "{name}" created, saved and activated.')

    @commands.command(name='set-ability', aliases=['character-set-ability', 'sa'], help='Set a character ability to a new value. Parameters: ability, rank, heroic dice, comma-seperated list of specializations')
    async def character_set_ability(self, ctx, ability, rank:int, heroic:int=None, specializations=None):
        if ctx.author.id in self.characters:
            character = self.characters[ctx.author.id]
            if not character["abilities"][ability]:
                await ctx.send(f'Invalid ability "{ability}".')
                return
            if rank < -5 or rank > 5:
                await ctx.send(f'Invalid ability value "{rank}". Must be between -5 and +5')
                return
            if heroic is not None and (heroic < 0 or heroic > 3):
                await ctx.send(f'Invalid heroic dice amount "{heroic}". Must be between 0 and 3')
                return

            if specializations:
                specializations = "".join([c for c in specializations if re.match(r'[,\w]', c)])
                specs = specializations.split(',')
                character["abilities"][ability] = { "rank": rank, "heroic": heroic, "specializations": specs}
            else:
                specs = None
                character["abilities"][ability] = { "rank": rank, "heroic": heroic }

            if heroic and heroic > 0 and specializations:
                character["abilities"][ability] = { "rank": rank, "heroic": heroic, "specializations": specs}
            elif heroic and heroic > 0:
                character["abilities"][ability] = { "rank": rank, "heroic": heroic }
            elif specializations:
                character["abilities"][ability] = { "rank": rank, "specializations": specs}
            else:
                character["abilities"][ability] = { "rank": rank }

            data = json.dumps(character, indent=3)
            try:
                async with aiofiles.open(f'./data/{ctx.author.id}/{character["shortname"]}.json', mode='w') as f:
                    contents = await f.write(data)
                    await f.close()
                    await ctx.send(f"Character data updated.")
            except:
                await ctx.send(f'Error: Unable to save updated character data.')
                raise
        else:
            await ctx.send(f"You don't have a character active right now, {ctx.author.name}. Use !character-load (short: !load) plus the name to load a character.")

    @commands.command(name='show', aliases=['character-show', 'char-show', 'charshow', 'cs'], help='Show currently active character.')
    async def character_show(self, ctx):
        if ctx.author.id in self.characters:
            character = self.characters[ctx.author.id]
            await self.write_char_sheet(character) # FIXME: actually no need to wait here, just have no idea how to start a real co-routine in Python
            classes = []
            for c in character["classes"]:
                classes.append(f'Level {c["level"]} {c["class"]}')
#            link = 'http://notveryprofessional.com/dragoneye/dicedragon/' + urllib.parse.quote_plus(character["name"]) + f'_{ctx.author.id}.html'
            link = 'https://lemuria.org/~tom/dd/' + urllib.parse.quote_plus(character["shortname"]) + f'_{ctx.author.id}.html'
            print(link)
            embed = discord.Embed(title=character["name"], description=', '.join(classes), color=0x2e2eb8, url=link)
            for name, value in character["abilities"].items():
                if value["rank"] > 0:
                    rank = "+"+str(value["rank"])
                elif value["rank"] < 0:
                    rank = "- "+str(abs(value["rank"]))
                else:
                    rank = "± 0"
                if "heroic" in value:
                    rank = rank + f' [{value["heroic"]}]'
                embed.add_field(name=name, value=rank)
            await ctx.send(embed=embed)
        else:
            await ctx.send(f"You don't have a character active right now, {ctx.author.name}. Use !character-load (short: !load) plus the name to load a character.")


    @commands.command(name='check', aliases=['ability-check'], help='Show currently active character.')
    async def ability_check(self, ctx, abilityname):
        if ctx.author.id in self.characters:
            roll_command = self.bot.get_command("ability")
            character = self.characters[ctx.author.id]

            ability = find_ability(abilityname, character["abilities"])
            if (ability):
                rank = character["abilities"][ability]["rank"]
                if "heroic" in character["abilities"][ability]:
                    heroic = character["abilities"][ability]["heroic"]
                else:
                    heroic = 0
                await ctx.invoke(roll_command, rank, heroic, character["name"], ability)
            else:
                await ctx.send(f'Invalid ability "{abilityname}".')
        else:
            await ctx.send(f"You don't have a character active right now, {ctx.author.name}. Use !character-load (short: !load) plus the name to load a character.")


# FIXME: this is now duplicated between here and dice.py
def roll_die(sides=10):
    return random.randint(1, sides)

def setup(bot):
    bot.add_cog(Character(bot))

def find_ability(input, abilities):
    match = None
    matches = 0

    for a in abilities:
        if input.lower() in a.lower():
            match = a
            matches += 1

    if matches == 1:
        return match

    close = get_close_matches(input, abilities.keys())
    if len(close) == 1:
        return close[0]

    return None

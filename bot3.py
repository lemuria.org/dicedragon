import discord
from discord.ext import commands
from dotenv import load_dotenv

import os
import asyncio
import asyncpg


async def run():
    print('Dice Dragon starting up...')

    # load environment
    load_dotenv()

    # connect to database
    login_data = {"user": os.getenv('DB_USER'), "password": os.getenv('DB_PASSWORD'), "database": os.getenv('DB_NAME'), "host": "127.0.0.1"}
    db = await asyncpg.create_pool(**login_data)

    description = "Dragon Eye discord bot"

    # An dieser Stelle alle benötigten Tabellen für den Bot erstellen (CREATE IF NOT EXISTS)
    # await db.execute("CREATE TABLE IF NOT EXISTS test_table(name text, id integer PRIMARY KEY);")

    bot = Bot(description=description, db=db)

    try:
        await bot.start(os.getenv('DISCORD_TOKEN'))
    except KeyboardInterrupt:
        await db.close()
        await bot.logout()


class Bot(commands.Bot):
    def __init__(self, **kwargs):
        super().__init__(
            command_prefix=commands.when_mentioned_or('/', '!'),
            description=kwargs.pop('description')
        )

        self.db = kwargs.pop('db')

        # Load cogs
        for filename in os.listdir("./cogs"):
            if filename.endswith(".py"):
                name = filename[:-3]
                # Comment to disable errorhandler module
                if name == 'errorhandler':
                    continue
                self.load_extension(f"cogs.{name}")

    # Die ganzen Events erstellen
    async def on_ready(self):
        print('logged in as user {}'.format(self.user.name))

    async def on_command_error(self, ctx, error):
        if isinstance(error, commands.CommandNotFound):
            pass
        if isinstance(error, commands.MissingRequiredArgument):
            pass




# run the bot
loop = asyncio.get_event_loop()
loop.run_until_complete(run())

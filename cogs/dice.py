import discord
from discord.ext import commands
import random
import re


class Dice(commands.Cog):
    def __init__(self, bot):
        self.bot = bot


    async def ability_roll(self, ctx, dice:int, keep_dice:int, keep='highest', charname=None, abilityname=None):
        roll = [roll_die() for _ in range(0, dice)]

        # sort asc or desc depending on whether we are going to keep the lowest or highest
        result = sorted(roll, reverse=(keep == 'highest'))

        outcome = result[:keep_dice]

        # check for critical success (a pair) or failure (all 1s)
        last = -1
        critical = False
        fail = True
        for d in outcome:
            if d != 1:
                fail = False
                if d == last:
                    critical = True
            last = d

        if critical:
            # critical success - add the next highest/lowest die as well
            if keep_dice == dice:
                # roll an additional die
                outcome.append(roll_die())
                outcome.sort()
            else:
                outcome = result[:keep_dice+1]
        elif fail:
            outcome = []
            for _ in range(0, keep_dice): # use _ instead of x because in Python that's how you show that you're not actually using the variable inside the loop
                outcome.append(0)

        dice_sum = sum(d for d in outcome)

        # use character and ability name if given
        if charname and abilityname:
            title = f'{charname} rolls {abilityname}:'
            print(f'{charname} rolled {abilityname}: {dice_sum} - {outcome}')
        else:
            title = 'Dragon Eye Ability Test for {}'.format(ctx.author.name)
            print(f'{ctx.author.name} rolled {dice}d10, keep {keep} {keep_dice}: {dice_sum} - {outcome}')

        embed = discord.Embed(title=title,
                              description=f'Rolling {dice}d10, keep {keep} {keep_dice}',
                              color=0x22a7f0)
        embed.add_field(name='Roll', value=format(roll), inline=False)
        embed.add_field(name='Outcome', value=format(outcome), inline=True)
        embed.add_field(name='Total', value=format(dice_sum), inline=True)
        if critical:
            embed.add_field(name='Additional Info', value='**Critical Success**', inline=True)
        elif fail:
            embed.add_field(name='Additional Info', value='**Critical Fail**', inline=True)

        await ctx.send(embed=embed)


    @commands.command(name='dice', aliases=['d'], help='Roll ability dice. Parameters: dice [power level, default 2] [character name] [ability].')
    async def ability_dice(self, ctx, dice:int, pl:int=2, charname=None, abilityname=None):
        if dice < 1 or dice > 6:
            await ctx.send('Invalid ability value "{}". Must be between 1 and 6'.format(dice))
            return
        if pl < 2 or pl > 5:
            await ctx.send('Invalid power level value "{}". Must be between 2 and 5'.format(pl))
            return

        await self.ability_roll(ctx, dice, pl, 'highest', charname, abilityname)


    @commands.command(name='ability', aliases=['a', 'ab'], help='Roll an ability check. Parameters: ability [no. of heroic dice].')
    async def ability_check(self, ctx, ability:int, heroic:int=0, charname=None, abilityname=None):
        if not -5 <= ability <= 5: # more Python-like variant of "if ability < -5 or ability > 5:"
            await ctx.send('Invalid ability value "{}". Must be between -5 and +5'.format(ability))
            return
        if not 0 <= heroic <= 5: # same as above, for "if heroic < 0 or heroic > 5:"
            await ctx.send('Invalid heroic dice amount "{}". Must be between 0 and 5 (3 + 2 ritual dice)'.format(heroic))
            return

        dice = 2 + abs(ability) + heroic
        keep_dice = 2 + heroic
        keep = 'lowest' if ability < 0 else 'highest'
        await self.ability_roll(ctx, dice, keep_dice, keep, charname, abilityname)


    @commands.command(name='roll', aliases=['r'], help='Roll dice. Simple 1d10 or 2d6 etc. test. No modifiers supported.')
    async def roll(self, ctx, input:str, fluff:str=None):
        d = re.match(r'(\d*)d(\d+)', input)
        if d:
            if d.group(1):
                dice = int(d.group(1))
            else:
                dice = 1
            sides = int(d.group(2))
            if sides < 2 or sides > 100:
                await ctx.send(f'Invalid number of dice sides "{sides}".')
                return
            if dice < 1 or dice > 20:
                await ctx.send(f'Invalid number of dice "{dice}".')
                return


            roll = [roll_die(sides) for _ in range(0, dice)]
            dice_sum = sum(d for d in roll)

            embed = discord.Embed(title='Dice roll for {}'.format(ctx.author.name),
                          description=f'Rolling {dice}d{sides}',
                          color=0x22a7f0)
            embed.add_field(name='results', value=format(roll), inline=True)
            embed.add_field(name='total', value=format(dice_sum), inline=True)
            await ctx.send(embed=embed)
            print(f'{ctx.author.name} rolled {input}: {dice_sum}')

        else:
            await ctx.send(f'Invalid dice roll instructions "{input}".')
            return

def roll_die(sides=10):
    return random.randint(1, sides)


def setup(bot):
    bot.add_cog(Dice(bot))

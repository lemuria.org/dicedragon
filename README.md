# DiceDragon

Discord dice-rolling bot for the Dragon Eye tabletop roleplaying game.


## Dependencies

    pip install discord.py
    pip install aiofiles


## Token

In order to connect to Discord, a token is required.
The bot assumes that the token is stored in .env - **which should not be a part of the repository** because if you check it in, you will share your bot access token with the world.

import os
import random
import discord
from discord.ext import commands
from dotenv import load_dotenv

load_dotenv()
token = os.getenv('DISCORD_TOKEN')

bot = commands.Bot(command_prefix='!')

@bot.command(name='ability', aliases=['a', 'ab'], help='Roll an ability check. Parameters: ability [no. of heroic dice].')
async def ability(ctx, ability:int, heroic:int=0):
    if ability < -5 or ability > 5:
        await ctx.send('Invalid ability value "{}". Must be between -5 and +5'.format(ability))
        return
    if heroic < 0 or heroic > 5:
        await ctx.send('Invalid heroic dice amount "{}". Must be between 0 and 5 (3 + 2 ritual dice)'.format(heroic))
        return

    dice = 2 + abs(ability) + heroic
    keep_dice = 2 + heroic
    if (ability < 0):
        keep = 'lowest'
    else:
        keep = 'highest'

    roll = []
    for x in range(0, dice):
        roll.append(roll_die())

    result = sorted(roll)
    if keep == 'highest':
        result.reverse()
    outcome = result[:keep_dice]

    # check for critical success (a pair) or failure (all 1s)
    last = -1
    critical = False
    fail = True
    for d in outcome:
        if d != 1:
            fail = False
            if d == last
                critical = True
        last = d

    if critical:
        # critical success - add the next highest/lowest die as well
        if keep_dice == dice:
            # roll an additional die
            outcome.append(roll_die())
            outcome.sort()
        else:
            outcome = result[:keep_dice+1]
    elif fail:
        outcome = []
        for x in range(0, keep_dice):
            outcome.append(0)

    # now sum up all our dice for the total
    sum = 0
    for d in outcome:
        sum += d


    embed = discord.Embed(title='**Dragon Eye** Ability Test for {}'.format(ctx.author.name),
                          description='Rolling {}d10, keep {} {}'.format(dice, keep, keep_dice),
                          color=0x22a7f0)
    embed.add_field(name='Roll', value=format(roll), inline=False)
    embed.add_field(name='Outcome', value=format(outcome), inline=True)
    embed.add_field(name='Total', value=format(sum), inline=True)
    if critical:
        embed.add_field(name='Additional Info', value='**Critical Success**', inline=True)
    elif fail:
        embed.add_field(name='Additional Info', value='**Critical Fail**', inline=True)

    await ctx.send(embed=embed)


def roll_die(sides=10):
    die = random.randint(1, sides)
    return die


bot.run(token)
